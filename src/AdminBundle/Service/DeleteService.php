<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Theme;
use AppBundle\Entity\Question;
use AppBundle\Entity\Questionnaire;
use AppBundle\Entity\Note;
use AppBundle\Entity\Answer;

class DeleteService {
    
    /**
     * @var EntityManagerInterface
     */
    private $em;
    
    public function __construct(EntityManagerInterface $entityManager){
        $this->em = $entityManager;
    }
    
    public function deleteTheme(Theme $theme){
        foreach($theme->getQuestionnaires() as $questionnaire){
            $this->deleteQuestionnaire($questionnaire);
        }
        $this->em->remove($theme);
        $this->em->flush();
    }
    
    public function deleteQuestionnaire(Questionnaire $questionnaire){
        foreach($questionnaire->getQuestions() as $question){
            $this->deleteQuestion($question);
        }
        foreach($questionnaire->getNotes() as $note){
            $this->deleteNote($note);
        }
        $this->em->remove($questionnaire);
        $this->em->flush();
    }
    
    public function deleteNote(Note $note ){
        $this->em->remove($note);
        $this->em->flush();
    }
    
    public function deleteQuestion(Question $question){
        foreach($question->getAnswers() as $answer){
            $this->deleteAnswer($answer);
        }
        $this->em->remove($question);
        $this->em->flush();
    }
    
    public function deleteAnswer(Answer $answer){
        $this->em->remove($answer);
        $this->em->flush();
    }
}
