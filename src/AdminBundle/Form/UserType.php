<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;



class UserType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('username',
                    TextType::class,
                    array(
                        'label' => "Nom d'utilistateur"
                    ))
                ->add('email',
                    EmailType::class,
                    array(
                        'label' => "Email"
                    ))
                ->add('submit',
                    SubmitType::class,
                    array());
    }
}
