<?php


namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Theme;

class QuestionnaireType extends AbstractType {
   
    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder
                ->add('name',
                    TextType::class,
                    array(
                        'label' => 'Nom'
                    ))
                ->add('description',
                    TextareaType::class,
                    array(
                        'label'=>'Description'
                    ))
                ->add('theme',
                    EntityType::class,
                    array(
                        'label'=> 'Theme',
                        'class' => Theme::class,
                        'choice_label' => 'name'
                    ))
                ->add('submit',
                    SubmitType::class,
                    array(
                        'label' => 'Valider'
                    ));
    }
    
}
