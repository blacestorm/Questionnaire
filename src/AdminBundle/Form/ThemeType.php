<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ThemeType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
            
        $builder    
                ->add('name',
                    TextType::class,
                    array(
                        'label' => 'Nom',
                        'required' => true,
                        'attr' => array(
                            'class' => 'form-control mr-sm-2', 
                        )
                    ))
                ->add('submit',
                    SubmitType::class,
                    array(
                        'label' => 'Valider', 
                        'attr' => array(
                            'class' => 'btn btn-outline-warning my-2 my-sm-0'
                        )
                    ));
    }
}
