<?php


namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\UserType;

class AdminUserController extends Controller {
    
    
    /**
     * @Route("/admin/user" , name="admin_user")
     */
    public function userAction()
    {
        $users = $this->getDoctrine()->getRepository("AppBundle:User")->findAll();
        
        return $this->render('admin/list/user.html.twig',array(
            'Users' => $users,
        ));
    }
    
    /**
     * @Route("/admin/user/add" , name="admin_user_add")
     */
    public function UserAddAction(){
        return $this->redirectToRoute('@fos_user_registration_register');
    }
    
    /**
     * @Route("/admin/user/edit/{idUser}" , name="admin_user_edit")
     */
    public function UserEditAction(Request $request, int $idUser){
        $userEdit = $this->createForm(UserType::class);
        $userEdit->handleRequest($request);
        
        if($userEdit->isValid() && $userEdit->isSubmitted()){
            $this->userEdit($userEdit->getData(),$idUser);
        }
        
        $user = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($idUser);
        $userEdit->get('username')->setData($user->getUsername());
        $userEdit->get('email')->setData($user->getEmail());
        
        
        return $this->render('admin/add/user.html.twig', array(
            'UserEdit' => $userEdit->createView()
        ));
    }
    
    /**
     * @Route("/admin/user/delete/{idUser}" , name="admin_user_delete")
     */
    public function UserDeleteAction(int $idUser){
        $user = $this->getDoctrine()->getRepository("AppBundle:User")->findOneById($idUser);
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        $this->redirectToRoute('admin_user');
    }
    
    /** Methode pour modifier un utilisateur */
    private function userEdit($data,$idUser){
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array( 'id'=>$idUser));
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        
        $userManager->updateUser($user);
        $this->getDoctrine()->getManager()->flush();
    }
    
      
    
}
