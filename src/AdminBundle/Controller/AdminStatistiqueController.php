<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminStatistiqueController extends Controller {
   
    /**
     * @Route("/admin/statistique" , name="admin_statistique")
     */
    public function statistiqueAction()
    {
        $users = $this->getDoctrine()->getRepository("AppBundle:User")->findAll();
        $questionnaire = $this->getDoctrine()->getRepository('AppBundle:Questionnaire')->findAll();
        return $this->render('admin/list/statistique.html.twig',array(
            'Users' => $users,
            'Questionnaires' => $questionnaire
        ));
    }
}
