<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\ThemeType;
use AppBundle\Entity\Theme;
 
class AdminThemeController extends Controller {
    
    /**
     * @Route("/admin/theme" , name="admin_theme")
     */
    public function themeAction()
    {
        $themes = $this->getDoctrine()->getRepository("AppBundle:Theme")->findAll();
        
        return $this->render('admin/list/theme.html.twig',array(
            'Themes' => $themes,
        ));
    }
    
    /**
     * @Route("/admin/theme/delete/{idTheme}", name="admin_theme_delete")
     */
    public function themeDeleteAction(int $idTheme){
        $theme = $this->getDoctrine()->getRepository("AppBundle:Theme")->findOneById($idTheme);
        $this->get('AdminBundle.Service.DeleteService')->deleteTheme($theme);
        return $this->redirectToRoute("admin_theme");
    }
    
    /**
     * @Route("/admin/theme/edit/{idTheme}", name="admin_theme_edit")
     */
    public function themeEditAction(Request $request, int $idTheme){
        $formTheme = $this->createForm(ThemeType::class);
        $formTheme->handleRequest($request);
        
        
        $theme = $this->getDoctrine()->getRepository("AppBundle:Theme")->findOneById($idTheme);
        if($formTheme->isSubmitted() && $formTheme->isValid()){
            $this->editTheme($formTheme->getData(),$theme);
            return $this->redirectToRoute("admin_theme");
        }
        
        $formTheme->get('name')->setData($theme->getName());
        return $this->render('admin/add/theme.html.twig', array(
            'themeAdd'=> $formTheme->createView(),
        )); 
    }
    
    /**
     * @Route("/admin/theme/add", name="admin_theme_add")
     */
    public function themeAddAction(Request $request){
        $formTheme = $this->createForm(ThemeType::class);
        $formTheme->handleRequest($request);
        
        if($formTheme->isSubmitted() && $formTheme->isValid()){
            $this->addTheme($formTheme->getData());
            return $this->redirectToRoute("admin_theme");
        }
        return $this->render('admin/add/theme.html.twig', array(
            'themeAdd'=> $formTheme->createView(),
        )); 
    }
    
    /** Methode pour modifier un Theme */
    private function editTheme($data,$theme){
        $theme->setName($data["name"]);
        
        $this->getDoctrine()->getManager()->persist($theme);
        $this->getDoctrine()->getManager()->flush();
    }
    
    /**Methode pour ajouter un nouveau Theme */
    private function addTheme($data){
        $theme = new Theme();
        $this->editTheme($data, $theme);
    }
 
    
}
