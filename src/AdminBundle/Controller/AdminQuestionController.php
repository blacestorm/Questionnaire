<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\QuestionType;
use AppBundle\Entity\Question;
use AppBundle\Entity\Answer;

class AdminQuestionController extends Controller {
    
    /**
     * @Route("/admin/questionnaire/question/add/{idQuestionnaire}" , name="admin_question_add")
     */
    public function questionAddAction(Request $request ,int $idQuestionnaire)
    {
        $questionForm = $this->createForm(QuestionType::class);
        $questionForm->handleRequest($request);
        
        if($questionForm->isSubmitted() && $questionForm->isValid()){
            $this->addQuestion($questionForm->getData(),$idQuestionnaire);
            return $this->redirectToRoute('admin_question_add', array('idQuestionnaire' => $idQuestionnaire));
        }
        
        return $this->render('admin/add/question.html.twig', array(
            'QuestionForm' => $questionForm->createView(),
        ));
    } 
    
    
    /**
     * @Route("/admin/questionnaire/question/edit/{idQuestion}" , name="admin_question_edit")
     */
    public function questionEditAction(Request $request,int $idQuestion){
        $questionForm = $this->createForm(QuestionType::class);
        $questionForm->handleRequest($request);
        
        $question = $this->getDoctrine()->getRepository('AppBundle:Question')->findOneById($idQuestion);
        
        if($questionForm->isSubmitted() && $questionForm->isValid()){
            $this->editQuestion($question,$questionForm->getData());
            return $this->redirectToRoute('admin_questionnaire_edit', array('idQuestionnaire' => $question->getQuestionnaire()->getId()));
        }
        
        $this->editForm($question, $questionForm);
        
        return $this->render('admin/add/question.html.twig', array(
            'QuestionForm' => $questionForm->createView(),
        ));
    }
    
    /**
     * @Route("/admin/questionnaire/question/delete/{idQuestion}" , name="admin_question_delete")
     */
    public function questionDeleteAction(int $idQuestion){
        $question = $this->getDoctrine()->getRepository("AppBundle:Question")->findOneById($idQuestion);
        $questionnaire = $question->getQuestionnaire();
        $this->get('AdminBundle.Service.DeleteService')->deleteQuestion($question);
        return $this->redirectToRoute("admin_questionnaire_edit",array("idQuestionnaire" => $questionnaire->getId()));   
    }
    
    /** Methode pour ajouter les donnees dans les champs du formulaire*/
    private function editForm($question, $questionForm){
        $questionForm->get('title')->setData($question->getTitle());
        $questionForm->get('type')->setData($question->getType());
        $answer = $question->getAnswers();
        $questionForm->get('answer')->setData($answer);
        if($answer != null){
            for($i=0;$i<count($answer);$i++){
                $questionForm->get('answer')[$i]->setData($answer[$i]->getResponce());
            }
        }
    }

    /** Methode pour modifier une Question */
    private function editQuestion($question,$data){
        $question->setTitle($data['title']);
        $question->setType($data['type']);
        
        if($question->getAnswers()!= null){
            foreach($question->getAnswers() as $answer){
                $answer->setResponce($data['answer'][0]);
                array_shift($data['answer']);
            }
        }
        foreach($data['answer'] as $answer){
            $a = new Answer();
            $a->setResponce($answer);
            $a->setQuestion($question);
            $this->getDoctrine()->getManager()->persist($a);
        }
        
        $this->getDoctrine()->getManager()->flush();
        
    }
   
    /** Methode pour ajouter une nouvelle Question  */
    private function addQuestion($data,$idQuestionnaire){
        $question = new Question();
        $questionnaire = $this->getDoctrine()->getRepository('AppBundle:Questionnaire')->findOneById($idQuestionnaire);
        $question->setNumQuestion(count($questionnaire->getQuestions())+1);
        $question->setQuestionnaire($questionnaire);
        $question->setTitle($data['title']);
        $question->setType($data['type']);
        $this->getDoctrine()->getManager()->persist($question);
        $this->getDoctrine()->getManager()->flush();
        $this->editQuestion($question, $data);          
    }
    
}
