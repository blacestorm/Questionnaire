<?php


namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\QuestionnaireType;
use AppBundle\Entity\Questionnaire;


class AdminQuestionnaireController extends Controller {
    
    /**
     * @Route("/admin/questionnaires" , name="admin_questionnaire")
     */
    public function questionnaireAction()
    {
        $questionnaires = $this->getDoctrine()->getRepository("AppBundle:Questionnaire")->findAll();
        return $this->render('admin/list/questionnaire.html.twig',array(
            "Questionnaires" => $questionnaires
        ));
    }
    
    /**
     * @Route("/admin/questionnaires/add" , name="admin_questionnaire_add")
     */
    public function questionnaireAddAction(Request $request)
    {
        $questionnaireForm = $this->createForm(QuestionnaireType::class);
        $questionnaireForm ->handleRequest($request);
        
        if($questionnaireForm->isSubmitted() && $questionnaireForm->isValid()){
            $idQuestionnaire=$this->addQuestionnaire($questionnaireForm->getData());
            return $this->redirectToRoute('admin_question_add',array('idQuestionnaire' => $idQuestionnaire));
        }
        
        return $this->render('admin/add/questionnaire.html.twig', array(
            'QuestionnaireForm' => $questionnaireForm->createView(),
        ));
    }
    
    
    /**
     * @Route("/admin/questionnaires/edit/{idQuestionnaire}" , name="admin_questionnaire_edit")
     */
    public function questionnaireEditAction(Request $request, int $idQuestionnaire)
    {
        $questionnaireForm = $this->createForm(QuestionnaireType::class);
        $questionnaireForm ->handleRequest($request);
        
        $questionnaire = $this->getDoctrine()->getRepository("AppBundle:Questionnaire")->findOneById($idQuestionnaire);
        
        if($questionnaireForm->isSubmitted() && $questionnaireForm->isValid()){
            $this->editQuestionnaire($questionnaire, $questionnaireForm->getData());
            $this->redirectToRoute('admin_questionnaire');
        }
        
        $this->editForm($questionnaireForm, $questionnaire);
        
        return $this->render('admin/edit/questionnaireEdit.html.twig',array(
            'Questionnaire' => $questionnaire,
            'QuestionnaireForm' => $questionnaireForm->createView()
        ));
    }
    
    /**
     * @Route("/admin/questionnaires/delete/{idQuestionnaire}" , name="admin_questionnaire_delete")
     */
    public function questionnaireDeleteAction(int $idQuestionnaire)
    {
        $questionnaire = $this->getDoctrine()->getRepository("AppBundle:Questionnaire")->findOneById($idQuestionnaire);
        $this->get('AdminBundle.Service.DeleteService')->deleteQuestionnaire($questionnaire);
        return $this->redirectToRoute("admin_questionnaire");
    }
    
    /** Methode pour ajouter une nouveau Questionnaire  */
    private function addQuestionnaire($data){
        $questionnaire = new Questionnaire();
        $this->editQuestionnaire($questionnaire,$data);
        return $questionnaire->getId();
    }
    
    /** Methode pour modifier un Questionnaire */
    private function editQuestionnaire($questionnaire,$data){
        $questionnaire->setName($data['name']);
        $questionnaire->setDescription($data['description']);
        $questionnaire->setTheme($data['theme']);
        $questionnaire->setTelechargement(0);
        
        $this->getDoctrine()->getManager()->persist($questionnaire);
        $this->getDoctrine()->getManager()->flush();
    }
    
    /** Methode pour ajouter les donnees dans les champs du formulaire*/
    private function editForm($questionnaireForm, $questionnaire){
        $questionnaireForm->get('name')->setData($questionnaire->getName());
        $questionnaireForm->get('description')->setData($questionnaire->getDescription());
        $questionnaireForm->get('theme')->setData($questionnaire->getTheme());
    }
    
}
