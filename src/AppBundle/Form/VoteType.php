<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class VoteType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add("rate",
                    ChoiceType::class,
                    array(
                        'label' => 'Note',
                        'choices' => array(
                            '0' => 0,
                            '0.5' => 0.5,
                            '1' => 1,
                            '1.5' => 1.5,
                            '2' => 2,
                            '2.5' => 2.5,
                            '3' => 3,
                            '3.5' => 3.5,
                            '4' => 4,
                            '4.5' => 4.5,
                            '5' => 5,
                        ),
                        'data' => '2.5'
                    ))
                ->add("submit",
                    SubmitType::class,
                    array(
                        'label' => 'Noter',
                    ));
    }
}
