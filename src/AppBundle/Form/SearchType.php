<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class SearchType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('search',
                    TextType::class,
                    array(
                        "label" => false,
                        "required" => false,
                    ))
                ->add('order',
                    ChoiceType::class,
                    array(
                        "label" => "trier par",
                        'choices' => array(
                            "Alphabetique" => "alpha",
                            "Note" => "note",
                        ),
                    ))
                ->add('submit',
                    SubmitType::class,
                    array(
                        'label' => "Recherche"
                    ));
    }
}
