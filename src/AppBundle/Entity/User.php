<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * One User have Many Note
     * 
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Note", mappedBy="user", cascade={"persist"})
     */
    private $notes;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="telechargement", type="integer", nullable=true)
     */
    private $telechargement=0;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    /**
     * Set Note
     * 
     * @param Collection $notes
     * 
     * @return user
     */
    public function setNotes($notes)
    {
        $this->notes=$notes;
        
        return $this;
    }
    
    /**
     * Get Notes
     * 
     * @return Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }
    
        /**
     * Set telechargement
     * 
     * @param int
     * 
     * @return User
     */
    public function setTelechargement($telechargement)
    {
        $this->telechargement=$telechargement;
        return $this;
    }
    
    /**
     * get telechargement
     * 
     * @return int
     */
    public function getTelechargement ()
    {
        return $this->telechargement;
    }
    
    public function getNoteMoyenne()
    {
        $total=0;
        $nbNote = count($this->getNotes());
        foreach($this->getNotes() as $note){
            $total += $note->getValue();
        }
        if( $nbNote == 0 ){
            return 0;
        }
        return $total/$nbNote;
    }
}