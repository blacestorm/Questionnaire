<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionnaire
 *
 * @ORM\Table(name="questionnaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionnaireRepository")
 */
class Questionnaire 
{
 
    /**
     * @var int
     * 
     * @ORM\Column(name="id", type="smallint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, unique=true)
     */
    private $name;
    
    /**
     * @var text
     * 
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * One Questionnaire have Many Question
     * 
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Question", mappedBy="questionnaire", cascade={"persist"})
     */
    private $questions;
    
    /**
     * One Questionnaire have Many Note
     * 
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Note", mappedBy="questionnaire", cascade={"persist"})
     * 
     */
    private $notes;
    
    /**
     * Many Question have One Theme
     * 
     * @var Theme
     * 
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="questionnaires", cascade={"persist"})
     * @ORM\JoinColumn(name="themeId",referencedColumnName="id")
     */
    private $theme;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="telechargement", type="integer")
     */
    private $telechargement;
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set name
     * 
     * @param string $name
     * 
     * @return Questionnaire
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set description
     * 
     * @param string $description
     * 
     * @return Questionnaire
     */
    public function setDescription($description)
    {
        $this->description=$description;
        
        return $this;
    }
    
    /**
     * get description
     * 
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set Question
     * 
     * @param Collection $questions
     * 
     * @retun Questionnaire
     */
    public function setQuestions($questions)
    {
        $this->questions=$questions;
        
        return $this;
    }
    
    /**
     * Get Question
     * 
     * @return Question
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    
    /**
     * Set Note
     * 
     * @param Collection $notes
     * 
     * @retun Questionnaire
     */
    public function setNotes($notes)
    {
        $this->notes=$notes;
        
        return $this;
    }
    
    /**
     * Get Notes
     * 
     * @return Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }
    
    /**
     * Set Theme
     * 
     * @param Theme
     * 
     * @return Questionnaire
     */
    public function setTheme($theme)
    {
        $this->theme=$theme;
        return $this;
    }
    
    /**
     * get Theme
     * 
     * @return Theme
     */
    public function getTheme ()
    {
        return $this->theme;
    }
    
    /**
     * Set telechargement
     * 
     * @param int
     * 
     * @return Questionnaire
     */
    public function setTelechargement($telechargement)
    {
        $this->telechargement=$telechargement;
        return $this;
    }
    
    /**
     * get telechargement
     * 
     * @return int
     */
    public function getTelechargement ()
    {
        return $this->telechargement;
    }
    
    public function getNoteMoyenne()
    {
        $total=0;
        $nbNote = count($this->getNotes());
        foreach($this->getNotes() as $note){
            $total += $note->getValue();
        }
        if($nbNote == 0){
            return 0;
        }
        return $total/$nbNote;
    }
    
}
