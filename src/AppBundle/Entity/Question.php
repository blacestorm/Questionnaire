<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     */
    private $type;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="num_question", type="integer")
     */
    private $num_question;

    /**
     * One Question have Many Answer
     * 
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Answer", mappedBy="question", cascade={"persist"})
     */
    private $answers;
            
    /**
     * Many Question have One Questionnaire
     * 
     * @var Questionnaire
     * 
     * @ORM\ManyToOne(targetEntity="Questionnaire", inversedBy="questions", cascade={"persist"})
     * @ORM\JoinColumn(name="QuestionnaireId",referencedColumnName="id")
     */
    private $questionnaire;
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Question
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * set num_question
     * 
     * @param int $index
     * 
     * @return Question 
     */
    public function setNumQuestion($index)
    {
        $this->num_question=$index;
        
        return $this;
    }
    
    /**
     * get num_question
     * 
     * @return int 
     */
    public function getNumQuestion()
    {
        return $this->num_question;
    }
    
    /**
     * Get answers
     * 
     * @return Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
    
    /**
     * Set answers
     * 
     * @param Collection $answers
     * 
     * @return Question
     */
    public function setAnswers($answers)
    {
        $this->answers=$answers;
        return $this;              
    }
    
    /**
     * Set Questionnaire
     * 
     * @param Questionnaire
     * 
     * @return Question
     */
    public function setQuestionnaire($questionnaire)
    {
        $this->questionnaire=$questionnaire;
        return $this;
    }
    
    /**
     * get Questionnaire
     * 
     * @return Questionnaire
     */
    public function getQuestionnaire ()
    {
        return $this->questionnaire;
    }
    
}

