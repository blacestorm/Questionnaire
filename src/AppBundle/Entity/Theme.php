<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="theme")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ThemeRepository")
 */
class Theme
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * One Theme have Many Questionnaire
     * 
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Questionnaire", mappedBy="theme", cascade={"persist"})
     * 
     */
    private $questionnaires;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Theme
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set Questionnaire
     * 
     * @param Collection $questionnaires
     * 
     * @return Theme
     */
    public function setQuestionnaires($questionnaires)
    {
        $this->questionnaires=$questionnaires;
        
        return $this;
    }
    
    /**
     * Get Questionnaires
     * 
     * @return Collection 
     */
    public function getQuestionnaires()
    {
        return $this->questionnaires;
    }
}

