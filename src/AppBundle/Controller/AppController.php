<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use AppBundle\Form\SearchType;
use DOMDocument;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Form\VoteType;
use AppBundle\Entity\Note;
use AppBundle\Entity\Questionnaire;

class AppController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $themes = $this->getDoctrine()->getRepository("AppBundle:Theme")->findAll();
        $searchform = $this->createForm(SearchType::class);
        $searchform->handleRequest($request);
        
        
        if($searchform->isValid() && $searchform->isSubmitted()){
            $questionnaires = $this->recherche($searchform->getData());
        }
        else{
            $questionnaires = $this->getDoctrine()->getRepository("AppBundle:Questionnaire")->findAll([],['name'=>'ASC']);
        }
        
        return $this->render('default/index.html.twig', [
            'Themes' => $themes,
            'Questionnaires' => $questionnaires,
            'SearchForm' => $searchform->createView(),
        ]);
    }
    
    /**
     * @Route("/questionnaire/{idQuestionnaire}", name="questionnaire")
     */
    public function questionnaireAction(int $idQuestionnaire,Request $request){
        $questionnaire = $this->getDoctrine()->getRepository('AppBundle:Questionnaire')->findOneById($idQuestionnaire);
        
        $voteform = $this->createForm(VoteType::class);
        $voteform->handleRequest($request);
        
        if($voteform->isSubmitted() && $voteform->isValid()){
            $this->rating($voteform->getData(),$questionnaire);
        }
        return $this->render('default/description.html.twig',array(
           'Questionnaire' => $questionnaire,
           'VoteForm' => $voteform->createView(),
        ));
    }
    
    /**
     * 
     * @Route("/questionnaire/{idQuestionnaire}/pdf", name="questionnairetopdf")
     */
    public function pdfAction(int $idQuestionnaire){
        
        $questionnaire = $this->getDoctrine()->getRepository('AppBundle:Questionnaire')->findOneById($idQuestionnaire);
        $this->statistiqueTelechargement($questionnaire);
        $html = $this->renderView('default/pdf.html.twig', array(
            'Questionnaire'  => $questionnaire
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'file.pdf'
        );
    }
    
    /**
     * 
     * @Route("/questionnaire/{idQuestionnaire}/xml", name="questionnairetoxml")
     */
    public function xmlAction(int $idQuestionnaire){
        $questionnaire = $this->getDoctrine()->getRepository("AppBundle:Questionnaire")->findOneById($idQuestionnaire);
        $this->statistiqueTelechargement($questionnaire);
        $fileName = $this->getParameter('Xml_directory')."/".$questionnaire->getName().".xml";
        file_put_contents($fileName, $this->questionnaireToXML($questionnaire));
        $file = new File($fileName);
        return $this->file($file);
    }
    
    
    /** Methode permettant de rechercher grace a un nom  un Theme ou un Questionnaire par ordre alphabetique ou par note*/
    private function recherche($data){
        $questionnaireRepo = $this->getDoctrine()->getRepository("AppBundle:Questionnaire");
        $theme = $this->getDoctrine()->getRepository("AppBundle:Theme")->findOneByName($data["search"]);
        switch($data['order']){
            case 'note':
                if($theme != null){
                    return $questionnaireRepo->findByThemeOrderByNote($theme);
                }
                if($data['search'] != null){
                    return $questionnaireRepo->findByNameOrderByNote($data['search']);
                }
                return $questionnaireRepo->findAllOrderByNote();
            case 'alpha':
                if($theme != null){
                    return $questionnaireRepo->findBy(['theme'=>$theme],['name'=>'ASC']);
                }
                if($data['search'] != null){
                    return $questionnaireRepo->findByName($data["search"],['name' => 'ASC']);
                }
                return $questionnaireRepo->findAll([],['name'=>'ASC']);
            default:
                $questionnaires = $questionnaireRepo->findAll([],['name'=>'ASC']);
        }
        return $questionnaires;
    }
    
    /** Methode pour creer un fichier XML correspondant a un questionnaire */
    private function questionnaireToXML ($questionnaire){
        $xml_doc = new DOMDocument('1.0','utf-8');
        $xml_doc->appendChild($xmlQuestionnaire=$xml_doc->createElement('questionnaire'));
        $xmlQuestionnaire->appendChild($xml_doc->createElement('titre', $questionnaire->getName()));
        $xmlQuestionnaire->appendChild($xml_doc->createElement('description', $questionnaire->getDescription()));
        $xmlQuestionnaire->setAttribute('théme',$questionnaire->getTheme()->getName());
        $xmlQuestionnaire->setAttribute('note',$questionnaire->getNoteMoyenne());
        foreach($questionnaire->getQuestions() as $question){
            $xmlQuestionnaire->appendChild($xmlQuestion=$xml_doc->createElement('question'));
            $xmlQuestion->appendChild($xml_doc->createElement('titre', $question->getTitle()));
            $xmlQuestion->setAttribute('type', $question->getType());
            foreach($question->getAnswers() as $answer){
                $xmlQuestion->appendChild($xml_doc->createElement('reponse',$answer->getResponce()));
            }
        }
        $xml_string = $xml_doc->saveXML();
        return $xml_string;
    }
    
    /** Methode permettant de mettre a jour les statistique des telechargement pour les utilisateur ainsi que les questionnaire */
    private function statistiqueTelechargement(Questionnaire $questionnaire){
        $user = $this->getUser();
        if($user->getTelechargement()== null){
            $user->setTelechargement(1);
        }else{
            $user->setTelechargement($user->getTelechargement()+1);
        }

        $questionnaire->setTelechargement($questionnaire->getTelechargement()+1);
    }
    
    /** Methode permettant d'assigner une note a un questionnaire faite par un utilisateur */
    private function rating($data,$questionnaire){
        $user = $this->getUser();
        $note = $this->getDoctrine()->getRepository("AppBundle:Note")->findOneBy(['questionnaire' => $questionnaire, 'user'=>$user]);
        dump($note);
        if($note != null){
            $note->setValue($data['rate']);
            
            dump($note);
        }
        else{
            $newnote = new Note();
            $newnote->setQuestionnaire($questionnaire);
            $newnote->setUser($user);
            $newnote->setValue($data['rate']);
            $this->getDoctrine()->getManager()->persist($newnote);
        }
        $this->getDoctrine()->getManager()->flush();
    }
}
