<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Theme;
use AppBundle\Entity\Questionnaire;
use AppBundle\Entity\Question;
use AppBundle\Entity\User;
use AppBundle\Entity\Note;
use AppBundle\Entity\Answer;

class AppFixtures extends Fixture {
    
    public function load(ObjectManager $manager){
        
        $themes=[];
        for ($i = 0; $i < 5; $i++) {
            $theme = new Theme();
            $theme->setName('Theme '.$i);
            $manager->persist($theme);
            array_push($themes, $theme);
        }
        
        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $user->setUsername('user');
        $user->setPlainPassword('user');
        $user->setEmail('user@gmail.com');
        $user->setEnabled(true);
        $manager->persist($user);
                
        $admin = new User();
        $admin->setEmail('admin@gmail.fr');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPlainPassword('admin');
        $admin->setUsername('admin');
        $admin->setEnabled(true);
        $manager->persist($admin);
        
        $manager->flush();
        
        $users=[$user,$admin];
                
        foreach ($themes as $theme){
            $this->questionnaire($manager,$theme,$users);
        }
    }
    
    public function notation(Questionnaire $questionnaire, Array $users, ObjectManager $manager){
        $difnote = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
        foreach($users as $user){
            $note = new Note();
            $note->setQuestionnaire($questionnaire);
            $note->setUser($user);
            $note->setValue($difnote[mt_rand(0,10)]);
            $manager->persist($note);
            
        }
        $manager->flush();
    }
    
    public function questionnaire(ObjectManager $manager,Theme $theme , Array $users){
        $questionnaires=[];
        $name = $theme->getName();
        for ($i = 0; $i < 5; $i++) {
            $questionnaire = new Questionnaire();
            $questionnaire->setName('Questionnaire'.$i.$name);
            $questionnaire->setDescription('Description'.$i*1000000);
            $questionnaire->setTheme($theme);
            $questionnaire->setTelechargement(mt_rand(0,100));
            $manager->persist($questionnaire);
            array_push($questionnaires, $questionnaire);
        }

        $manager->flush();
        
        foreach ($questionnaires as $questionnaire){
            $this->question($manager,$questionnaire);
            $this->notation($questionnaire, $users, $manager);
        }
    }
    
        public function question(ObjectManager $manager,Questionnaire $questionnaire){
        $questions=[];
        $types=['multiple','numeric','ordonancee','unique'];
        for ($i = 0; $i < 5; $i++) {
            $question = new Question();
            $question->setTitle('Question '.$i);
            $question->setNumQuestion($i);
            $question->setType($types[mt_rand(0,3)]);
            $question->setQuestionnaire($questionnaire);
            $manager->persist($question);
            array_push($questions, $question);
        }

        $manager->flush();
        
        foreach ($questions as $question){
            $this->answer($manager,$question);
        }
    }
    
    public function answer(ObjectManager $manager, Question $question){
        for($i = 0;$i<mt_rand(0,10);$i++){
            $answer = new Answer();
            $answer->setResponce('Responce'.$i);
            $answer->setQuestion($question);
            $manager->persist($answer);
        }
        
        $manager->flush();
    }
}
