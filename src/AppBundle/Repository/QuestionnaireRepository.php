<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Theme;

class QuestionnaireRepository extends \Doctrine\ORM\EntityRepository {
    
    public function findAllOrderByNote(){
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT q
            FROM AppBundle\Entity\Questionnaire q, AppBundle\Entity\Note n
            WHERE q.id = n.questionnaire
            GROUP BY q.name
            HAVING AVG(n.value) =   (SELECT AVG(no.value)
                                    FROM AppBundle\Entity\Note no
                                    WHERE q.id = no.questionnaire
                                    GROUP BY q.id)
            ORDER BY AVG(n.value) DESC
            '
            );
        return $query->execute();
    }
    
    public function findByThemeOrderByNote(Theme $theme){
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT q
            FROM AppBundle\Entity\Questionnaire q, AppBundle\Entity\Note n
            WHERE q.id = n.questionnaire
            AND q.theme = :theme
            GROUP BY q.name
            HAVING AVG(n.value) =   (SELECT AVG(no.value)
                                    FROM AppBundle\Entity\Note no
                                    WHERE q.id = no.questionnaire
                                    GROUP BY q.id)
            ORDER BY AVG(n.value) DESC'
            );
        $query->setParameter('theme', $theme);
        return $query->execute();
    }
    
    public function findByNameOrderByNote(String $name){
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT q
            FROM AppBundle\Entity\Questionnaire q, AppBundle\Entity\Note n
            WHERE q.id = n.questionnaire
            AND q.name = :name
            GROUP BY q.name
            HAVING AVG(n.value) =   (SELECT AVG(no.value)
                                    FROM AppBundle\Entity\Note no
                                    WHERE q.id = no.questionnaire
                                    GROUP BY q.id)
            ORDER BY AVG(n.value) DESC'
            );
        $query->setParameter('name', $name);
        return $query->execute();
    }
    
    
}
