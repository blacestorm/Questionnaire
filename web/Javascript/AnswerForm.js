$(document).ready(function() {
    var $container = $('#question_answer');
    var index = $container.find(':input').length;
    
    $('#add_question_answer').click(function(e) {
        addAnswer($container);
        e.preventDefault(); 
        return false;
    });

    if (index === 0) {
    } else {
        $container.children('div').each(function() {
            addDeleteLink($(this));
        });
    }

    function addAnswer($container) {
        var template = $container.attr('data-prototype')
            .replace(/__name__label__/g, 'Réponse n°' + (index+1))
            .replace(/__name__/g, index);
        var $prototype = $(template);
        addDeleteLink($prototype);
        $container.append($prototype);
        index++;
    }

    function addDeleteLink($prototype) {
        var $deleteLink = $('<a href="#" class="btn btn-danger">Supprimer</a>');
        $prototype.append($deleteLink);
        $deleteLink.click(function(e) {
            $prototype.remove();
            e.preventDefault();
            index--;
            return false;
        });
    }
});